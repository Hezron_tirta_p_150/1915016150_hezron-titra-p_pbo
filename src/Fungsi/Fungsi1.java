
package Fungsi;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Fungsi1 {
    public static int errorinthandling(){
        Scanner input = new Scanner(System.in);
        while(true){
            try{
                return input.nextInt();
            }
            catch (InputMismatchException e){
                input.nextLine();
                System.out.print("masukkan angka : ");
            }
        }
    }
    public static void clearConsol(){
        try{
            String os = System.getProperty("os.name");
            if(os.contains("Windows")){
                Runtime.getRuntime().exec("cls");
                }
            else{
                Runtime.getRuntime().exec("cls");
            }
        }
        catch(IOException e){}
    }
    public static void idr(){
           DecimalFormat kursindonesi = (DecimalFormat)DecimalFormat.getCurrencyInstance();
           DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
           
           formatRp.setCurrencySymbol("Rp.");
           formatRp.setMonetaryDecimalSeparator(',');
           formatRp.setGroupingSeparator('.');
           kursindonesi.setDecimalFormatSymbols(formatRp);
    
    }
}
