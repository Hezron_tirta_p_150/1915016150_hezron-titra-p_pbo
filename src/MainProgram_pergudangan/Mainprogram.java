package MainProgram_pergudangan;
import pergudangan.*;
import static Fungsi.Fungsi1.*;
import java.util.Scanner;
import java.util.ArrayList;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;


public class Mainprogram {
    public static void main(String[] args) {
        ArrayList<Ragam_makanan_ringan>ListRagam_makanan_ringan = new ArrayList<>();
        ArrayList<Ragam_minyak_goreng>ListRagam_minyak_goreng = new ArrayList<>();
        Scanner input = new Scanner(System.in);
        int a,b,i,c,d,e,f,g,h,indeks,pilih,indeks1;
        DecimalFormat kursindonesi = (DecimalFormat)DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
        
        OUTER :
                while(true){
                System.out.println("=============================");
                System.out.println("|        Pilihan            |");
                System.out.println("-----------------------------");
                System.out.println("| 1. Tambah data barang     |");
                System.out.println("| 2. Tampilkan daftar barang|");
                System.out.println("| 3. Hapus data barang      |");
                System.out.println("| 4. Mencari data barang    |");
                System.out.println("| 5. Exit                   |");
                System.out.println("=============================");
                System.out.print(" masukkan pilihan (1-5) : ");
                pilih = errorinthandling();
                if (pilih == 1){
                    OUTER1 :
                        while(true){
                            Ragam_makanan_ringan DATA = new Ragam_makanan_ringan();
                            Ragam_minyak_goreng DATA1 = new Ragam_minyak_goreng();
                            System.out.println("-----Pilihan barang------");
                            System.out.println("-------------------------");
                            System.out.println("|1. Makanan ringan      |");
                            System.out.println("|2. Minyak goreng       |");
                            System.out.println("|3. kembali kemenu utama|");
                            System.out.println("|-----------------------|");
                            System.out.print(" masukkan pilihan : ");
                            b = errorinthandling();
                            switch(b){
                                case 1:
                                    System.out.println("--Penambahan data makanan ringan--");
                                    System.out.print("Nama barang             : ");
                                    DATA.setNama(input.next());
                                    System.out.print("Kode barang             : ");
                                    DATA.setIDbrg(errorinthandling());
                                    System.out.print("Harga barang            : Rp.");
                                    DATA.sethargabarang(errorinthandling());
                                    System.out.print("Jumlah barang           : ");
                                    DATA.setjmlbarang(errorinthandling());
                                    System.out.print("Isi/berat barang (gram) : ");
                                    DATA.setisiberat(errorinthandling());
                                    System.out.print("Expire/kadaluarsa(year) : ");
                                    DATA.setexpire(errorinthandling());
                                    ListRagam_makanan_ringan.add(DATA);
                                    System.out.println("penambahan data succes");
                                    break;
                                case 2 :
                                    System.out.println("--Penambahan data minyak goreng--");
                                    System.out.print("Nama barang              : ");
                                    DATA1.setNama(input.next());
                                    System.out.print("Kode barang              : ");
                                    DATA1.setIDbrg(errorinthandling());
                                    System.out.print("Harga barang             :Rp. ");
                                    DATA1.sethargabarang(errorinthandling());
                                    System.out.print("Jumlah barang            : ");
                                    DATA1.setjmlbarang(errorinthandling());
                                    System.out.print("Isi/berat barang (mL)    : ");
                                    DATA1.setisiberat(errorinthandling());
                                    System.out.print("Expire/kadaluarsa (year) : ");
                                    DATA1.setexpire(errorinthandling());
                                    ListRagam_minyak_goreng.add(DATA1);
                                    System.out.println("penambahan data succes");
                                    break;
                                case 3 :
                                    clearConsol();
                                    break OUTER1;
                                default:
                                    System.out.println(" maaf input anda salah");
                                    break;        
                            }
                        }

                    }
                else if (pilih == 2){
                    OUTER2 :
                    while(true){
                            System.out.println("pilih barang yang ditampilkan");
                            System.out.println("-------------------------");
                            System.out.println("|1. Makanan ringan      |");
                            System.out.println("|2. Minyak goreng       |");
                            System.out.println("|3. kembali kemenu utama|");
                            System.out.println("|-----------------------|");
                            System.out.print(" masukkan pilihan : ");
                            a = errorinthandling();
                            switch(a){                           
                                case 1:
                                    if (ListRagam_makanan_ringan.isEmpty()) {
                                    System.out.println("-------------------------");
                                    System.out.println("    Data barang Kosong   ");
                                    System.out.println("  silahkan masukkan data ");
                                    System.out.println("-------------------------");
                                    } 
                                    else{
                                    System.out.println("-----Tampilan barang------");
                                    System.out.println("NO   Nama \t Kode barang \t jumlah barang");
                                    for(i=0;i<ListRagam_makanan_ringan.size();i+=1){
                                    System.out.println(i+"    "+ListRagam_makanan_ringan.get(i).getNama()+"   \t "+ListRagam_makanan_ringan.get(i).getIDbrg()+"  \t "+ListRagam_makanan_ringan.get(i).getjmlbarang());
                                    }
                                    System.out.println("--------------------------");
                                    }
                                    break;
                                case 2:
                                    if (ListRagam_makanan_ringan.isEmpty()) {
                                    System.out.println("-------------------------");
                                    System.out.println("    Data barang Kosong   ");
                                    System.out.println("  silahkan masukkan data ");
                                    System.out.println("-------------------------");                                    
                                    } 
                                    else{
                                    System.out.println("-----Tampilan barang------");
                                    System.out.println("NO   Nama \t Kode barang  \t jumlah barang");
                                    for(i=0;i<ListRagam_minyak_goreng.size();i+=1){
                                    System.out.println(i+"    "+ListRagam_minyak_goreng.get(i).getNama()+"   \t "+ListRagam_minyak_goreng.get(i).getIDbrg()+"  \t "+ListRagam_minyak_goreng.get(i).getjmlbarang());
                                    }
                                    System.out.println("--------------------------");
                                    }
                                    break;
                                case 3:
                                    clearConsol();
                                    break OUTER2;
                                default:
                                    System.out.println(" maaf input anda salah");
                                    break;
                    }
                }
            }
                else if (pilih == 3) {
                    OUTER3 :
                    while(true){
                        System.out.println("pilih barang yang dihapus");
                        System.out.println("-------------------------");
                        System.out.println("|1. Makanan ringan      |");
                        System.out.println("|2. Minyak goreng       |");
                        System.out.println("|3. kembali kemenu utama|");
                        System.out.println("|-----------------------|");
                        System.out.print(" masukkan pilihan : ");
                        c = errorinthandling();
                        switch(c){
                            case 1:
                                System.out.println("--------------------------");
                                System.out.println("NO   Nama \t Kode barang \t jumlah barang");
                                for(i=0;i<ListRagam_makanan_ringan.size();i+=1){
                                System.out.println(i+"    "+ListRagam_makanan_ringan.get(i).getNama()+"   \t "+ListRagam_makanan_ringan.get(i).getIDbrg()+"  \t "+ListRagam_makanan_ringan.get(i).getjmlbarang());
                                }
                                System.out.println("--------------------------");
                                System.out.print("Masukkan NO data yang akan dihapus : ");
                                indeks = errorinthandling();
                                if (indeks < ListRagam_makanan_ringan.size()){
                                ListRagam_makanan_ringan.remove(indeks);
                                System.out.println("data telah dihapus");
                                }
                                else{
                                    System.out.println("\n maaf No yang anda masukkan salah");
                                    }
                                break;
                            case 2:
                                System.out.println("--------------------------");
                                System.out.println("NO   Nama \t Kode barang  \t jumlah barang");
                                for(i=0;i<ListRagam_minyak_goreng.size();i+=1){
                                System.out.println(i+"    "+ListRagam_minyak_goreng.get(i).getNama()+"   \t "+ListRagam_minyak_goreng.get(i).getIDbrg()+"  \t "+ListRagam_minyak_goreng.get(i).getjmlbarang());
                                }
                                System.out.println("--------------------------");
                                System.out.print("Masukkan NO data yang akan dihapus : ");
                                indeks1 = errorinthandling();
                                if (indeks1 <ListRagam_minyak_goreng.size()){
                                ListRagam_minyak_goreng.remove(indeks1);
                                System.out.println("data telah dihapus");
                                }
                                else{
                                    System.out.println("\n maaf No yang anda masukkan salah");
                                    }
                                break;
                            case 3 :
                                    clearConsol();
                                    break OUTER3;
                            default:
                                    System.out.println(" maaf input anda salah");
                                    break;
                        }
                    }
                }
                else if (pilih == 4){
                    OUTER4 :
                    while(true){
                        System.out.println("pilih barang yang dicari");
                        System.out.println("-------------------------");
                        System.out.println("|1. Makanan ringan      |");
                        System.out.println("|2. Minyak goreng       |");
                        System.out.println("|3. kembali kemenu utama|");
                        System.out.println("|-----------------------|");
                        System.out.print(" masukkan pilihan : ");
                        d = errorinthandling();
                        switch(d){
                            case 1 :
                                formatRp.setCurrencySymbol("Rp.");
                                formatRp.setMonetaryDecimalSeparator(',');
                                formatRp.setGroupingSeparator('.');
                                kursindonesi.setDecimalFormatSymbols(formatRp);
                                System.out.print("Masukkan kode barang : ");
                                e = errorinthandling();
                                for(f=0;f<ListRagam_makanan_ringan.size();f++){
                                    if((e)==ListRagam_makanan_ringan.get(f).getIDbrg()){
                                        System.out.println("|---------------------------------------|");
                                        System.out.println("|Nama barang             : "+ListRagam_makanan_ringan.get(f).getNama());
                                        System.out.printf("|Harga barang             : %s %n",kursindonesi.format(ListRagam_makanan_ringan.get(f).gethargabarang()));
                                        System.out.println("|jumlah barang           : "+ListRagam_makanan_ringan.get(f).getjmlbarang());
                                        System.out.println("|isi/berat               : "+ListRagam_makanan_ringan.get(f).getisiberat()+" (kg)");
                                        System.out.println("|Expire/kadaluarsa (year): "+ListRagam_makanan_ringan.get(f).getexpire());
                                        System.out.println("|---------------------------------------|");
                                    }
                                    else {
                                        System.out.println("\n maaf No yang anda masukkan salah");
                                    }
                                }break;
                            case 2:
                                formatRp.setCurrencySymbol("Rp.");
                                formatRp.setMonetaryDecimalSeparator(',');
                                formatRp.setGroupingSeparator('.');
                                kursindonesi.setDecimalFormatSymbols(formatRp);
                                System.out.print("Masukkan kode barang : ");
                                g=errorinthandling();
                                for(h=0;h<ListRagam_minyak_goreng.size();h++){
                                    if((g)==ListRagam_minyak_goreng.get(h).getIDbrg()){
                                        System.out.println("|---------------------------------------|");
                                        System.out.println("|Nama barang             : "+ListRagam_minyak_goreng.get(h).getNama());
                                        System.out.printf("|Harga barang             : %s %n",kursindonesi.format(ListRagam_minyak_goreng.get(h).gethargabarang()));
                                        System.out.println("|jumlah barang           : "+ListRagam_minyak_goreng.get(h).getjmlbarang());
                                        System.out.println("|isi/berat               : "+ListRagam_minyak_goreng.get(h).getisiberat()+" (kg)");
                                        System.out.println("|Expire/kadaluarsa (year): "+ListRagam_minyak_goreng.get(h).getexpire());
                                        System.out.println("|---------------------------------------|");
                                    }
                                    else {
                                        System.out.println("\n maaf No yang anda masukkan salah");
                                    }
                                }break;
                            case 3 :
                                    clearConsol();
                                    break OUTER4;
                            default:
                                    System.out.println(" maaf input anda salah");
                                    break;
                        }
                    }
                }
                else if (pilih == 5){
                    System.out.println("exit program");
                    break;
                } else {
                    System.out.println("pilihan anda tidak tersedia,pilihan yang tesedia ( 1-5 )");
                }
        }
    }
}

 